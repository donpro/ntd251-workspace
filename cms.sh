CMS_RAILS_PATH=~/viki-projects/baboon
CMS_EMBER_V2_PATH=$CMS_RAILS_PATH/app/assets/javascripts/v2
CMS_EMBER_V3_PATH=$CMS_RAILS_PATH/app/assets/javascripts/v3
DEPLOY_PATH=~/viki-projects/ops/src/deploys
CAPRICORN_PATH=~/viki-projects/cp_panel
KUNKKA_CHARTS_PATH=~/viki-projects/kunkka/charts

alias rg='RAILS_ENV=test rails g'
alias generate-ember-component='RAILS_ENV=test rails g ember_component'
alias gec='generate-ember-component'

# makefile autocomplete
#
complete -W "\`grep -oE '^[a-zA-Z0-9_.-]+:([^=]|$)' Makefile | sed 's/[^a-zA-Z0-9_.-]*$//'\`" make

function restart_db_staging {
  ssh -t db.staging "sudo service pgpool2 restart"
}

function cms_open_ember() {
  case "$1" in
    v2)
      code $CMS_EMBER_V2_PATH
      ;;
    help)
      echo "1. cms ember"
      echo "2. cms ember v2"
      ;;
    *)
      code $CMS_EMBER_V3_PATH
  esac
}

function sports-elasticsearch-staging() {
  gcloud container clusters get-credentials sports-staging-central --region asia-southeast1 --project sports-central-staging-k8s  && kubectl port-forward --namespace staging $(kubectl get pod --namespace staging --selector="app=internal-elasticsearch,component=client,pod-template-hash=3897787048,release=internal-elasticsearch" --output jsonpath='{.items[0].metadata.name}') 9200:9200
}

function ksh() {
  app=$1
  container=$2
  pod_id=$3

  if [ "$container" = "" ]; then
    container="$app"
  fi

  if [ "$pod_id" = "" ]; then
    pod_id=$(kubectl get pod --namespace staging --selector="app=${app}" --output jsonpath='{.items[0].metadata.name}')
  fi

  echo "ksh to pod $pod_id"

  kubectl exec -it $pod_id -n staging -c ${container} -- bash
}

function ksh-production() {
  app=$1
  container=$2
  pod_id=$3

  if [ "$container" = "" ]; then
    container="$app"
  fi

  if [ "$pod_id" = "" ]; then
    pod_id=$(kubectl get pod --namespace production --selector="app=${app}" --output jsonpath='{.items[0].metadata.name}')
  fi

  echo "ksh-production to pod $pod_id"

  kubectl exec -it $pod_id -n production -c ${container} -- bash
}

function klogs-f() {
  app=$1
  container=$2

  if [ "$container" = "" ]; then
    container="$app"
  fi

  pod_id=$(kubectl get pod --namespace staging --selector="app=${app}" --output jsonpath='{.items[0].metadata.name}')

  kubectl logs -f $pod_id -n staging -c ${container}
}

function kforward-pod() {
  app=$1
  container=$app
  ports=$2

  pod_id=$(kubectl get pod --namespace staging --selector="app=${app}" --output jsonpath='{.items[0].metadata.name}')

  kubectl port-forward --namespace staging $pod_id $ports
}

function kforward-service() {
  service=$1
  ports=$2

  kubectl port-forward -n staging service/${service} $ports
}

alias helm-v2.10.0=~/Downloads/helm-v2.10.0/helm
alias helm-v2.11.0=~/Downloads/helm-v2.11.0/helm

function kdeploy-kunkka() {
  release=$1
  tag=$2

  # currently helm v2.11 in staging
  #
  kunkka_path='/Users/duongtien.nguyen/viki-projects/kunkka'
  ~/Downloads/helm-v2.10.0/helm upgrade --install --namespace staging --set-string image.tag=$tag $release ${kunkka_path}/charts/${release}/
}

function kdeploy-kunkka-new-staging() {
  release=$1
  tag=$2

  # currently helm v2.11 in staging
  #
  kunkka_path='/Users/duongtien.nguyen/viki-projects/kunkka'
  ~/Downloads/helm-v2.10.0/helm upgrade --install --namespace staging --set-string image.tag=$tag $release ${kunkka_path}/charts/new-central-charts/${release}/
}

function kdeploy-kunkka-production() {
  release=$1
  tag=$2

  # currently helm v2.11 in staging
  #
  kunkka_path='/Users/duongtien.nguyen/viki-projects/kunkka'
  ~/Downloads/helm-v2.10.0/helm upgrade --install --namespace production --set-string image.tag=$tag $release ${kunkka_path}/charts/${release}/
}

function kdeploy-kunkka-new-production() {
  release=$1
  tag=$2

  # currently helm v2.11 in staging
  #
  kunkka_path='/Users/duongtien.nguyen/viki-projects/kunkka'
  ~/Downloads/helm-v2.10.0/helm upgrade --install --namespace production --set-string image.tag=$tag $release ${kunkka_path}/charts/new-central-charts/${release}/
}

function kdeploy-production() {
  release=$1
  tag=$2

  kunkka_path='/Users/duongtien.nguyen/viki-projects/kunkka'

  ~/Downloads/helm-v2.10.0/helm upgrade --install --namespace production --set-string image.tag=$tag $release ./charts/
  # helm upgrade --install --namespace staging --set-string image.tag=$tag $release ./charts/
}

function cms_deploy_prod() {
  # save current folder to return after the whole process
  path=`pwd`

  # get master HEAD commit version
  cd $CMS_RAILS_PATH
  git fetch
  new_version=`git rev-parse --short origin/master`

  # go to deploy folder
  cd $DEPLOY_PATH

  # checking current version
  app_status=`fab status:production,cms,app1.dal`
  current_version=`echo $app_status | cut -d " " -f 4 | cut -d ":" -f 2`


  if [ $new_version = $current_version ];then
    echo "Already latest version"
  else
    echo "Deploying..."
  fi

  # Go back to original folder
  cd $path


  #git pull && fab deploy:production,cms,app1.dal,$current_version,$current_version
  #fab status:production,cms,app1.dal
}

function cms_deploy() {
  case "$1" in
    production)
      cms_deploy_prod
      ;;

    prod)
      cms_deploy production
      ;;

    *)
      echo "Wrong environment"
  esac
}

function git_merges() {
  repo=$1
  commit=$2

  git log $commit.. \
    --merges --first-parent master \
    --pretty=format:"%h %<(15,trunc)%an %<(15)%ar %<(25,trunc)%s   https://github.com/viki-org/${repo}/commit/%h"
}

function git_remove_merged_branches() {
  git checkout master
  git branch --merged master | grep -v ^\* | xargs git branch -d
}

function cms() {
  case "$1" in
    server)
      cd $CMS_RAILS_PATH
      rails s
      ;;

    # SOURCE CODE
    rails)
      code $CMS_RAILS_PATH
      ;;

    ember)
      cms_open_ember $2
      ;;

    capricorn)
      code $CAPRICORN_PATH
      ;;
    # TEST
    rspec)
      cd $CMS_RAILS_PATH
      ./vrspec $2
      ;;
    karma)
      cd $CMS_RAILS_PATH
      ./karma.sh
      ;;

    deploy)
      cms_deploy $2 $3
      ;;

    # SERVERS
    prod)
      vsh app1.dal cms-1
      ;;
    prod2)
      vsh app2.dal cms-1
      ;;
    staging)
      vsh app1.staging cms-1
      ;;
    dev)
      vsh app1.dev cms-1
      ;;
    worker)
      vsh workers.dal cms-worker
      ;;
    worker-staging)
      vsh w-central.staging cms-worker
      ;;
    w-staging)
      cms worker-staging
      ;;
    worker-dev)
      vsh w-central.dev cms-worker
      ;;
    w-dev)
      cms worker-dev
      ;;
    asana)
      open "https://app.asana.com"
      ;;
    add)
      file_path="$2"
      cms validate "$file_path"
      git add "$file_path"
      git st
      ;;
    validate)
      file_path="$2"
      if [[ $file_path == *".rb" ]]; then
        cms validate_rb "$file_path"
      fi
      ;;
    validate_rb)
      file_path="$2"
      echo "------"
      echo "Validating $file_path"
      echo "------"
      ruby -c "$file_path"
      rubocop -a "$file_path"
      ;;
    build_push_deploy_staging)
      version="$2"
      perform-at $CMS_RAILS_PATH make build_docker version=$version
      perform-at $CMS_RAILS_PATH make push_docker version=$version
      perform-at $DEPLOY_PATH fab deploy:staging,cms,app1.staging,$version,$version,remove
      ;;
    merges)
      perform-at $CMS_RAILS_PATH git_merges $2
      ;;
    clean)
      perform-at $CMS_RAILS_PATH git_remove_merged_branches
      ;;
    help)
      echo "1. cms server"
      echo "2. cms rails"
      echo "3. cms ember"
      echo "4. cms ember v2"
      echo "5. cms rspec"
      echo "6. cms karma"
      ;;
    *)
      perform-at $CMS_RAILS_PATH "$@"
      ;;
  esac
}