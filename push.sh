git add .

if [ -n "$1" ]
then
  git commit -m "$@"
else
  git commit -m "Updated commands"
fi

git push origin master
