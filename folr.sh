FOLR_RAILS_PATH=~/sugar-projects/folr
FOLR_EMBER_PATH=~/sugar-projects/folr-web-app-business
  
function announce() {
  echo
  echo "$@"
  echo
}

function folr_ssh() {
  if [ "$1" = "" ]; then
    folr_ssh help
  else
    message="Connecting to $1 server... Please wait..."
    case "$1" in
      staging)
        announce "$message"
        ssh ubuntu@54.254.143.74
        ;;
      staging-us)
        announce "$message"
        ssh ubuntu@54.165.162.169
        ;;
      production)
        announce "$message"
        ssh ubuntu@54.169.200.198
        ;;
      prod)
        folr_ssh production
        ;;
      help)
        echo "Choose one of the following env:"
        echo "1. folr ssh staging"
        echo "2. folr ssh staging-us"
        ;;
      *)
        echo "...Invalid Server..."
        folr_ssh help
    esac
  fi
}

function folr_start() {
  case "$1" in
    all)
      folr_start_servers
      folr_start_browser
      ;;
    servers)
      folr_start_servers
      ;;
    browser)
      folr_start_browser
      ;;
    coding)
      folr_start_coding
      ;;
    help)
      echo "Choose option:"
      echo "1. all"
      echo "2. staging"
      echo "3. browser"
      echo "4. coding"
      echo "E.g. folr start all"
      ;;
    *)
      echo "folr start help"
  esac
}

function folr_start_servers() {
  folr_start_rails_server
  folr_start_middleman_server
}

function folr_start_rails_server() {
  cd $FOLR_RAILS_PATH
  bundle exec rails s
}

function folr_start_middleman_server() {
  cd $FOLR_EMBER_PATH/v1
  middleman server
}

function folr_start_ember_server() {
  cd $FOLR_EMBER_PATH/v2
  ember server
}

function folr_node_server() {
  cd $FOLR_RAILS_PATH/nodejs
  node index
}

function folr_start_browser() {
  # open jira
  open "https://rhodesedge.atlassian.net/projects/FOLR/issues"
  # open slack
  open "https://rhodesedge.slack.com"
  # open harvest
  open "https://rhodesedge.harvestapp.com"
}

function folr_goto() {
  case "$1" in
    rails)
      cd $FOLR_RAILS_PATH
      ;;
    middleman)
      cd $FOLR_EMBER_PATH
      ;;
    ember)
      cd $FOLR_EMBER_PATH
      ;;
  esac
}

function folr_start_coding() {
  subl $FOLR_EMBER_PATH/v1
  spectacle "Sublime Text" "left"
  subl $FOLR_EMBER_PATH/v2/app
  spectacle "Sublime Text" "right"
  subl $FOLR_RAILS_PATH
  spectacle "Sublime Text" "left"
}

function folr() {
  case "$1" in
    ssh)
      folr_ssh $2
      ;;
    start)
      folr_start $2
      ;;
    browser)
      folr_start_browser
      ;;
    code)
      folr_start_coding
      ;;
    middleman)
      folr_start_middleman_server
      ;;
    ember)
      folr_start_ember_server
      ;;
    node)
      folr_node_server
      ;;
    rails)
      folr_start_rails_server
      ;;
    cd)
      folr_goto $2
      ;;
    help)
      echo "1. ssh"
      echo "2. start"
      echo "3. browser"
      echo "4. code"
      echo "5. middleman"
      echo "6. rails"
      echo "7. cd"
      echo "8. help"
      ;;
    *)
      folr help
  esac
}