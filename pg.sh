function pg() {
  case "$1" in
    db.sng)
      psql -h db02.sng.viki.io -U baboon -p 7778
      ;;
    db.dev)
      psql -h db.dev.viki.io -U baboon -p 9999
      ;;
    db.staging)
      psql -h db.staging.viki.io -U baboon -p 5432
      ;;
    db.subber-staging)
      psql -h db.staging.viki.io -U subber -p 5432
      ;;
    db.prod)
      psql -h db-master.prod.viki.io -U baboon -p 9998
      ;;
    db.subber-prod)
      psql -h db-master.prod.viki.io -U subber -p 9998
      ;;
  esac
}
