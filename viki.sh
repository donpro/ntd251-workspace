function vsh() {
  if [ "$1" = "" ] || [ "$2" = "" ]; then
    echo
    echo "PRODUCTION      :  vsh app1.dal cms"
    echo "STAGING         :  vsh app1.staging cms"
    echo "DEV             :  vsh app1.dev cms"
    echo "WORKER          :  vsh workers.dal cms-worker"
    echo "WORKER STAGING  :  vsh w-central.staging cms-worker"
    echo "WORKER DEV      :  vsh w-central.dev cms-worker"
    echo
  else
    ssh -t $1 "docker exec -it \`docker ps -a|grep $2|awk '{print \$1}'|head -n1\` bash -c \"${3:-bash}\""
  fi
}

function vsh() {
  ssh -t $1 "docker exec -it \`docker ps -a|grep $2|awk '{print \$1}'|head -n1\` bash -c \"${3:-bash}\""
}

function lb.dal.credentails() {
  ssh lb.dal grep admin: /etc/haproxy/haproxy.cfg
}

function vault-github-token() {
  cat ~/.vault-github-token
}

function vault-login() {
  echo "---------------------------"
  echo "Vault login for $VAULT_ADDR"
  echo "---------------------------"
  vault login -method=github token="$(vault-github-token)"
}

function consul-login() {
  team=$1
  env=$2

  consul_token=$(vault read -field=token consul/creds/${team}-${env})
  export CONSUL_HTTP_TOKEN="$consul_token"
}

function consul-token() {
  team=$1
  env=$2

  file_path="$(consul-file-path $team $env)"

  touch $file_path
  token="$(cat $file_path)"

  if [ -z "$token" ]
  then
    vault read -field=token consul/creds/${team}-${env} > $file_path
    cat $file_path
  else
    echo "$token"
  fi
}

function consul-env() {
  team=$1
  env=$2

  token=`consul-token $1 $2`
  export CONSUL_HTTP_TOKEN=$token
}

function consul-token-refresh() {
  echo "---------------------------"
  echo "Vault login for $CONSUL_HTTP_ADDR"
  echo "---------------------------"

  team=$1
  env=$2

  remove-consul-token $1 $2
  consul-token $1 $2
}

function remove-consul-token() {
  team=$1
  env=$2
  file_path="$(consul-file-path $team $env)"
  rm $file_path
}

function consul-file-path() {
  team=$1
  env=$2
  echo "${HOME}/.consul-${team}-${env}"
}

function consul-upload() {
  rb_path="/Users/duongtien.nguyen/viki-projects/ops/src/consul/gcp/vault/migration/uploadConsulKV.rb"
  ruby $rb_path $1
}

function vault-upload() {
  rb_path="/Users/duongtien.nguyen/viki-projects/ops/src/consul/gcp/vault/migration/uploadVaultKV.rb"
  ruby $rb_path $1
}
