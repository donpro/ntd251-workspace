export TERMINAL_CMD_PATH=~/terminal-cmd
export DEPLOY_PATH=~/viki-projects/ops/src/deploys
export PERSONAL_NOTES_PATH=~/side-projects/ntd251-personal-notes
export SUBBER_PATH=~/viki-projects/subber
export WORKSPACE_SETUP_PATH=~/viki-projects/workspace-setup

function subl() {
  /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl $1
}

function reload() {
  echo
  echo "-----------------------"
  echo "Reloading .bash_profile"
  echo "-----------------------"
  echo

  source ~/.bash_profile
}

function open_new_tab() {
  osascript -e 'tell application "Terminal" to activate' -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down'
}

# spectacle "Sublime Text" "right"
function spectacle() {
  case "$2" in
    left)
      key=123
      ;;
    right)
      key=124
      ;;
    down)
      key=125
      ;;
    up)
      key=126
      ;;
  esac

  osascript -e "tell application \"$1\" to activate" -e "tell application \"System Events\" to tell process \"Terminal\" to key code $key using {command down, option down}"
}

function edit() {
  terminal_commands=("helpers" "cms" "folr" "capricorn")
  case "$1" in
    bash)
      subl ~/.bash_profile
      ;;
    notes)
      subl $PERSONAL_NOTES_PATH
      ;;
    note)
      edit notes
      ;;
    terminal-cmd)
      subl $TERMINAL_CMD_PATH
      ;;
    tc)
      edit terminal-cmd
      ;;
    setup)
      subl $WORKSPACE_SETUP_PATH
      ;;
    *)
      if [[ " ${terminal_commands[@]} " =~ " $1 " ]]; then
        subl $TERMINAL_CMD_PATH/$1.sh
      else
        echo "Invalid args for edit command"
        echo Try: ${terminal_commands[@]} "bash"
      fi
      ;;
  esac
}

###
# TERMINAL COMMANDS
###

function terminal-cmd() {
  case "$1" in
    edit)
      edit terminal-cmd
    ;;
    push)
      push-terminal-cmd "${@:2}"
    ;;
    view)
      open "https://bitbucket.org/donpro/ntd251-workspace/src"
    ;;
    open)
      terminal-cmd view
    ;;
    *)
      perform-at $TERMINAL_CMD_PATH "$@"
    ;;
  esac
}

function push-terminal-cmd() {
  perform-at $TERMINAL_CMD_PATH ./push.sh "$@"
}

function tc() {
  terminal-cmd "$@"
}

# Perform something at some other location
#
function perform-at() {
  current_path=`pwd`
  target_path=$1

  cd $target_path
  ${@:2}

  cd $current_path
}

function hey() {
  cd ~/viki-projects/heybot
  node index.js
}

function goto() {
  case "$1" in
    cms)
      cd $CMS_RAILS_PATH
      ;;
    capricorn)
      cd $CAPRICORN_RAILS_PATH
      ;;
    cpp)
      goto capricorn
      ;;
    subber)
      cd $SUBBER_PATH
      ;;
    deploy)
      cd $DEPLOY_PATH
      ;;
    notes)
      cd $PERSONAL_NOTES_PATH
      ;;
    note)
      cd $PERSONAL_NOTES_PATH
      ;;
    tc)
      cd $TERMINAL_CMD_PATH
      ;;
    setup)
      cd $WORKSPACE_SETUP_PATH
      ;;
    terminal-cmd)
      goto tc
      ;;
  esac
}

###
# NOTES
###

function note() {
  case "$1" in
    cd)
      goto notes
      ;;
    goto)
      goto notes
      ;;
    edit)
      edit notes
      ;;
    push)
      push_note ${@:2}
      note view
      ;;
    view)
      open "https://bitbucket.org/donpro/ntd251-personal-notes"
      ;;
    open)
      note view
      ;;
    *)
      perform-at $PERSONAL_NOTES_PATH "$@"
    ;;
  esac
}

function push_note() {
  perform-at $PERSONAL_NOTES_PATH ./push.sh "$1"
}

function notes() {
  note "$@"
}

###
# OTHERS
###

function set_terminal_text () {
    # echo works in bash & zsh
    local mode=$1 ; shift
    echo -ne "\033]$mode;$@\007"
}

set_both  () { set_terminal_text 0 "$@"; }
tab   () { set_terminal_text 1 "$@"; }
window () { set_terminal_text 2 "$@"; }
