export LAST_PROFILE_PATH=~/.last_profile
export VIKI_PROFILE_PATH=~/.viki_bash_profile
export DESS_PROFILE_PATH=~/.dess_bash_profile

function login-as() {
  profile="$1"
  load-profile $profile
  cache-profile $profile
  echo
}

function perform-as() {
  profile="$1"
  action="$2"

  if [ -n "$action" ]
  then
    echo
    echo "Performing as $profile"
    echo "----------------------"
    echo "> ${@:2}"
    echo "----------------------"
    load-profile $profile
    ${@:2} # perform action
    load-last-profile
    echo "----------------------"
    echo
  else
    login-as $profile
  fi
}

function viki() {
  perform-as viki "$@"
}

function dess() {
  perform-as dess "$@"
}

function cache-profile() {
  echo $1 > $LAST_PROFILE_PATH
}

function load-profile() {
  profile="$1"

  case $profile in
    viki)
      source $VIKI_PROFILE_PATH
      ;;
    dess)
      source $DESS_PROFILE_PATH
      ;;
    *)
      source $VIKI_PROFILE_PATH
      ;;
  esac

  update-shell-label
}

function login-as-last-profile() {
  login-as `cat $LAST_PROFILE_PATH`
}

function load-last-profile() {
  load-profile `cat $LAST_PROFILE_PATH`
}
